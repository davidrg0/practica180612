const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

function writeUserDateToFile(data){
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err) {
      if (err){
        console.log(err);
      } else{
        console.log("Datos escritos en fichero.");
      }
    }
  );
}

app.get('/apitechu/v1/hello',
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde API TechU!"});
  }
);

app.get('/apitechu/v1/users',
  function(req, res){
    console.log("GET /apitechu/v1/users");

    var users = require('./usuarios.json');
    var respuesta = users;

    if(typeof req.query.$top !== 'undefined'){
      var respuesta=users.slice(0,req.query.$top);
    }

    if(typeof req.query.$count !== 'undefined' && req.query.$count == "true"){
      respuesta.push({"count" : users.length});
    }

    res.send(respuesta);
  }
);

app.post('/apitechu/v1/users',
  function(req, res){
    console.log("POST /apitechu/v1/users");
    console.log("first_name es " + req.body.first_name);
    console.log("last_name es " + req.body.last_name);
    console.log("email es   " + req.body.email);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email
    };
    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDateToFile(users);

    console.log("Usuario añadido con éxto");

    res.send({"msg" : "Usuario añadido con éxto"});
  }
);

app.delete('/apitechu/v1/users/:id',
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("id es " + req.params.id);

    var users = require('./usuarios.json');
    var deleteOK = false;
    for (let user of users) {
      if(user.id == req.params.id){
        var index = users.indexOf(user);
        users.splice(index,1);
        writeUserDateToFile(users);
        console.log("Usuario borrado " + JSON.stringify(user));
        res.send({"msg" : "Usuario borrado"});
        deleteOK = true;
        break;  //Asuminos que solo hay 1 objeto con ese id
      }
    }

  //   users.forEach(function(user, index){
  //     if(user.id == req.params.id){
  //       users.splice(index,1);
  //       writeUserDateToFile(users);
  //       console.log("Usuario borrado " + JSON.stringify(user));
  //       res.send({"msg" : "Usuario borrado"});
  //       deleteOK = true;
  //     }
  //   }
  // )

    if (!deleteOK){
      console.log("El usuario no existe!");
      res.status(404).send({"msg" : "El usuario no existe!"});
    }
  }
);

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query string");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
);

app.post('/apitechu/v1/login',
  function(req, res){
    console.log("POST /apitechu/v1/login");

    var users = require('./usuarios.json');
    for (user of users) {
      if(user.email == req.body.email && user.password == req.body.password){
        user.logged = true;
        writeUserDateToFile(users);
        res.send({"mensaje" : "Login correcto", "idUsuario" : user.id});
        break;
      }
    }
    res.send({"mensaje" : "Login incorrecto"});
  }
);

app.post('/apitechu/v1/logout',
  function(req, res){
    console.log("POST /apitechu/v1/logout");

    var users = require('./usuarios.json');
    for (user of users) {
      if(user.id == req.body.id && user.logged == true){
        delete user.logged;
        writeUserDateToFile(users);
        res.send({"mensaje" : "Logout correcto", "idUsuario" : user.id});
        break;
      }
    }
    res.send({"mensaje" : "Logout incorrecto"});
  }
);
